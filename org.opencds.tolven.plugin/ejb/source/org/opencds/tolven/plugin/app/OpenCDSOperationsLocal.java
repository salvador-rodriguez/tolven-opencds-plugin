/**
 * 
 */
package org.opencds.tolven.plugin.app;

import org.tolven.app.entity.MenuData;
import org.tolven.app.entity.MenuStructure;
import org.tolven.core.entity.AccountUser;

/**
 * Methods in this interface are used for OpenCDS related operations.
 * 
 * @author
 */

public interface OpenCDSOperationsLocal {


	/**
	 * 
	 * @return
	 */
	public String getTitle();



	public void opencdsServiceRequest(AccountUser accountUser, MenuData md,
			MenuStructure msLabresults, MenuStructure msProcedures,
			MenuStructure msProblems, MenuStructure msAssessments)
			throws Exception;
}
