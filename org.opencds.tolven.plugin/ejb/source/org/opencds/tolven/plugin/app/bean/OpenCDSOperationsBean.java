package org.opencds.tolven.plugin.app.bean;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.omg.spec.cdss._201105.dss.EvaluateAtSpecifiedTime;
import org.omg.spec.cdss._201105.dss.EvaluateAtSpecifiedTimeResponse;
import org.omg.spec.cdss._201105.dss.EvaluationResponse;
import org.omg.spec.cdss._201105.dss.FinalKMEvaluationResponse;
import org.omg.spec.cdss._201105.dsswsdl.DecisionSupportService;
import org.omg.spec.cdss._201105.dsswsdl.Evaluation;
import org.opencds.tolven.plugin.app.OpenCDSOperationsLocal;
import org.opencds.tolven.plugin.cdsinput.CDSInput;
import org.opencds.tolven.plugin.cdsinput.ENXP;
import org.opencds.tolven.plugin.cdsinput.EncounterEvent;
import org.opencds.tolven.plugin.cdsinput.EntityNameUse;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.ClinicalStatements.EncounterEvents;
import org.opencds.tolven.plugin.cdsinput.II;
import org.opencds.tolven.plugin.cdsinput.IVLTS;
import org.opencds.tolven.plugin.cdsinput.ObjectFactory;
import org.opencds.tolven.plugin.cdsinput.ObservationResult;
import org.opencds.tolven.plugin.cdsinput.ObservationResult.ObservationValue;
import org.opencds.tolven.plugin.cdsinput.PQ;
import org.opencds.tolven.plugin.cdsinput.Problem;
import org.opencds.tolven.plugin.cdsinput.ProcedureEvent;
import org.opencds.tolven.plugin.cdsinput.TS;
import org.opencds.tolven.plugin.cdsinput.VMR;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.ClinicalStatements;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.Demographics;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.ClinicalStatements.ObservationResults;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.ClinicalStatements.Problems;
import org.opencds.tolven.plugin.cdsinput.EvaluatedPerson.ClinicalStatements.ProcedureEvents;
import org.tolven.app.MenuLocal;
import org.tolven.app.bean.CreatorBean;
import org.tolven.app.entity.MenuData;
import org.tolven.app.entity.MenuStructure;
import org.tolven.core.entity.AccountUser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class handles the OpenCDS related operations
 * 
 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
 */

@Stateless()
@Local(OpenCDSOperationsLocal.class)
public class OpenCDSOperationsBean extends CreatorBean implements
		OpenCDSOperationsLocal {

	Logger logger = Logger.getLogger(this.getClass());

	@EJB
	MenuLocal menuBean;

	private String title;
	private String text;
	private String effectiveTime;
	private String reviewed;
	private String sent;
	private String recommendedAction;
	private String recommendedActionTaken;

	private static final QName SERVICE_NAME = new QName(
			"http://www.omg.org/spec/CDSS/201105/dssWsdl",
			"DecisionSupportService");

	@XmlElement(required = true)
	protected List<ENXP> part;
	@XmlAttribute
	protected List<EntityNameUse> use;

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param value
	 * @return
	 */
	private TS setTS(String value) {
		ObjectFactory factory = new ObjectFactory();
		TS ts = factory.createTS();

		if (value != null) {
			ts.setValue(value);
			return ts;
		} else {
			throw new RuntimeException("root attribute is required");
		}

	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param lowIsInclusive
	 * @param highIsInclusive
	 * @param high
	 * @param low
	 * @return
	 */
	private org.opencds.tolven.plugin.cdsinput.IVLTS setIVLTS(
			Boolean lowIsInclusive, Boolean highIsInclusive, String high,
			String low) {
		ObjectFactory factory = new ObjectFactory();
		IVLTS ivlst = factory.createIVLTS();

		if (lowIsInclusive != null) {
			ivlst.setLowIsInclusive(lowIsInclusive);
		}
		if (highIsInclusive != null) {
			ivlst.setHighIsInclusive(highIsInclusive);
		}
		if (high != null) {
			ivlst.setHigh(high);
		}
		if (low != null) {
			ivlst.setLow(low);
		}

		return ivlst;
	}

	
	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param unit
	 * @param value
	 * @return
	 */
	private org.opencds.tolven.plugin.cdsinput.PQ setPQ(
			Double value,
			String unit) {
		
		ObjectFactory factory = new ObjectFactory();
		PQ pq = factory.createPQ();

		if (unit != null) {
			pq.setUnit(unit);
		}
		if (value != null) {
			pq.setValue(value);
		}

		return pq;
	}
	
	
	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param code
	 * @param codeSystem
	 * @param codeSystemName
	 * @param displayName
	 * @param originalText
	 * @return
	 */
	private org.opencds.tolven.plugin.cdsinput.CD setCD(
			String code,
			String codeSystem, 
			String codeSystemName, 
			String displayName,
			String originalText) {
		ObjectFactory factory = new ObjectFactory();
		org.opencds.tolven.plugin.cdsinput.CD cd = factory.createCD();

		if (code != null) {
			cd.setCode(code);
		}

		if (codeSystem != null) {
			cd.setCodeSystem(codeSystem);
		}

		if (codeSystemName != null) {
			cd.setCodeSystemName(codeSystemName);
		}

		if (displayName != null) {
			cd.setDisplayName(displayName);
		}

		if (originalText != null) {
			cd.setOriginalText(originalText);
		}

		return cd;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param root
	 * @param extension
	 * @return
	 */
	private org.opencds.tolven.plugin.cdsinput.II setII(String root,
			String extension) {
		ObjectFactory factory = new ObjectFactory();
		org.opencds.tolven.plugin.cdsinput.II ii = factory.createII();

		if (root != null) {
			ii.setRoot(root);
			if (extension != null) {
				ii.setExtension(extension);
			}
			return ii;
		} else {
			throw new RuntimeException("root attribute is required");
		}
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param accountUser
	 * @param md
	 * @param msProblems
	 * @return
	 */
	Problems buildVmrProblems(AccountUser accountUser, MenuData md,
			MenuStructure msProblems) {

		String highDate;
		String lowDate;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		ObjectFactory factory = new ObjectFactory();
		String accountId = String.valueOf(accountUser.getAccount().getId());
		String path = md.getPath();
		List<MenuData> problemList = menuBean.findListContents(
				accountUser.getAccount(), msProblems, md.getParent01());
		Problems problems = factory
				.createEvaluatedPersonClinicalStatementsProblems();

		for (MenuData problem : problemList) {
			Problem prob = factory.createProblem();
			List<II> probTemplateIdList = prob.getTemplateId();
			probTemplateIdList.add(setII("2.16.840.1.113883.3.795", ""));
			prob.setId(setII("1.2.1." + accountId, problem.getPath()));
			prob.setDataSourceType(setCD("Clinical", "2.16.840.1.113883.6.96",
					"SNOMED-CT", "", ""));
			prob.setProblemCode(setCD(problem.getString06(),
					"2.16.840.1.113883.6.96", "SNOMED-CT",
					problem.getString01(), null));
			highDate = df.format(problem.getUpdatetime());
			lowDate = df.format(problem.getDate01());
			prob.setProblemEffectiveTime(setIVLTS(null, null, highDate, lowDate));
			problems.getProblem().add(prob);
		}

		if (path.contains(md.getParentPath01() + ":problem")) {
			Problem problem = factory.createProblem();
			List<II> probTemplateIdList = problem.getTemplateId();
			probTemplateIdList.add(setII("2.16.840.1.113883.3.795", ""));
			problem.setId(setII("1.2.1." + accountId, md.getPath()));
			problem.setDataSourceType(setCD("Clinical",
					"2.16.840.1.113883.6.96", "SNOMED-CT", "", ""));
			problem.setProblemCode(setCD(md.getString06(),
					"2.16.840.1.113883.6.96", "SNOMED-CT", md.getString01(),
					null));
			highDate = df.format(md.getUpdatetime());
			lowDate = df.format(md.getDate01());
			problem.setProblemEffectiveTime(setIVLTS(null, null, highDate,
					lowDate));
			problems.getProblem().add(problem);
		}

		return problems;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param accountUser
	 * @param md
	 * @param msLabresults
	 * @return
	 */
	ObservationResults buildVmrLabResults(AccountUser accountUser,
			MenuData md, MenuStructure msLabresults) {

		String highDate;
		String lowDate;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		ObjectFactory factory = new ObjectFactory();
		String accountId = String.valueOf(accountUser.getAccount().getId());
		String path = md.getPath();

		List<MenuData> observationList = menuBean.findListContents(
				accountUser.getAccount(), msLabresults, md.getParent01());
		ObservationResults observationResults = factory
				.createEvaluatedPersonClinicalStatementsObservationResults();

		for (MenuData observation : observationList) {
			ObservationResult observationResult = factory
					.createObservationResult();
			
			observationResult.setId(setII("1.2.1." + accountId,
					observation.getPath()));
			
			observationResult.setObservationFocus(setCD(
					observation.getString03(), "1.3.6.1.4.1.12009.10.2.3",
					"LOINC", observation.getString02(), null));
			highDate = df.format(observation.getDate01());
			lowDate = df.format(observation.getDate01());
			
			observationResult.setObservationEventTime(setIVLTS(null, null,
					highDate, lowDate));
			
			ObservationValue value = factory.createObservationResultObservationValue();
			value.setPhysicalQuantity(setPQ(md.getPqValue01(), md.getPqUnits01()));
			observationResult.setObservationValue(value);
			
			observationResults.getObservationResult().add(observationResult);
		}
		if (path.contains(md.getParentPath01() + ":labresult")) {
			ObservationResult observationResult = factory
					.createObservationResult();
			observationResult.setId(setII("1.2.1." + accountId, md.getPath()));
			observationResult
					.setObservationFocus(setCD(md.getString03(),
							"1.3.6.1.4.1.12009.10.2.3", "LOINC",
							md.getString02(), null));
			highDate = df.format(md.getDate01());
			lowDate = df.format(md.getDate01());

			observationResult.setObservationEventTime(setIVLTS(null, null,
					highDate, lowDate));
			
			ObservationValue value = factory.createObservationResultObservationValue();
			value.setPhysicalQuantity(setPQ(md.getPqValue01(), md.getPqUnits01()));
			observationResult.setObservationValue(value);
			
			observationResults.getObservationResult().add(observationResult);
		}

		return observationResults;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param accountUser
	 * @param md
	 * @param msProcedureList
	 * @return
	 */
	ProcedureEvents buildVmrProcedureEvents(AccountUser accountUser,
			MenuData md, MenuStructure msProcedureList) {

		String highDate;
		String lowDate;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		ObjectFactory factory = new ObjectFactory();
		String accountId = String.valueOf(accountUser.getAccount().getId());
		String path = md.getPath();

		List<MenuData> proceduresList = menuBean.findListContents(
				accountUser.getAccount(), msProcedureList, md.getParent01());
		ProcedureEvents proceduresEvents = factory
				.createEvaluatedPersonClinicalStatementsProcedureEvents();

		for (MenuData procedure : proceduresList) {
			ProcedureEvent procedureEvent = factory.createProcedureEvent();
			List<II> procedureTemplateIdList = procedureEvent.getTemplateId();
			procedureTemplateIdList.add(setII("2.16.840.1.113883.3.795", ""));
			procedureEvent.setId(setII("1.2.3." + accountId,
					procedure.getPath()));
			procedureEvent.setDataSourceType(setCD("Clinical",
					"2.16.840.1.113883.3.795", "", "", ""));
			procedureEvent.setProcedureCode(setCD(procedure.getString06(),
					"2.16.840.1.113883.6.104", "ICD-9CM",
					procedure.getString01(), null));
			highDate = df.format(procedure.getUpdatetime());
			lowDate = df.format(procedure.getDate01());
			procedureEvent.setProcedureTime(setIVLTS(null, null, highDate,
					lowDate));
			proceduresEvents.getProcedureEvent().add(procedureEvent);
		}

		if (path.contains(md.getParentPath01() + ":px-")) {
			ProcedureEvent procedureEvent = factory.createProcedureEvent();
			procedureEvent.setId(setII("1.2.1." + accountId, md.getPath()));
			procedureEvent.setProcedureCode(setCD(md.getString06(),
					"2.16.840.1.113883.6.104", "ICD-9CM", md.getString01(),
					null));
			highDate = df.format(md.getUpdatetime());
			lowDate = df.format(md.getDate01());
			procedureEvent.setProcedureTime(setIVLTS(null, null, highDate,
					lowDate));
			proceduresEvents.getProcedureEvent().add(procedureEvent);
		}

		return proceduresEvents;
	}

	
	
	//TODO - this method is not ready
	EncounterEvents buildVmrEncounterEvents(AccountUser accountUser,
			MenuData md, MenuStructure msEncounterList) {

		String highDate;
		String lowDate;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		// Mandatory fields: id, EncounterType, encounterEventTime

		ObjectFactory factory = new ObjectFactory();
		String accountId = String.valueOf(accountUser.getAccount().getId());
		String path = md.getPath();

		List<MenuData> encounterEventsList = menuBean.findListContents(
				accountUser.getAccount(), msEncounterList, md.getParent01());
		EncounterEvents encounterEvents = factory
				.createEvaluatedPersonClinicalStatementsEncounterEvents();

		for (MenuData encounter : encounterEventsList) {
			EncounterEvent encounterEvent = factory.createEncounterEvent();
			List<II> encounterTemplateIdList = encounterEvent.getTemplateId();

			encounterTemplateIdList.add(setII("1.2.1.117001", "")); // TODO get
																	// the
																	// systemCode,
																	// codeSystemVersion

			encounterEvent.setId(setII("1.2.1." + accountId, encounterEvent
					.getId().getExtension())); // TODO get the Encounter event
												// Id

			encounterEvent.setDataSourceType(setCD("Clinical",
					"2.16.840.1.113883.3.795", "", "", ""));

			// encounterEvent.setEncounterCode(setCD(encounterEvent.getString06(),
			// "2.16.840.1.113883.6.104", "ICD-9CM",
			// encounterEvent.getString01(), null));

			encounterEvent.setEncounterEventTime(encounterEvent
					.getEncounterEventTime());
			encounterEvents.getEncounterEvent().add(encounterEvent);
		}

		if (path.contains(md.getParentPath01() + ":px-")) {
			EncounterEvent encounterEvent = factory.createEncounterEvent();
			encounterEvent.setId(setII("1.2.1." + accountId, md.getPath()));
			encounterEvent.setEncounterType(setCD(md.getString06(),
					"2.16.840.1.113883.6.104", "ICD-9CM", md.getString01(),
					null));
			highDate = df.format(md.getUpdatetime());
			lowDate = df.format(md.getDate01());
			encounterEvent.setEncounterEventTime(setIVLTS(null, null, highDate,
					lowDate));
			encounterEvents.getEncounterEvent().add(encounterEvent);
		}

		return encounterEvents;
	}
	
	
	
	ObservationResults buildVmrAssessments(AccountUser accountUser,
			MenuData md, MenuStructure msAssessments) {

		String highDate;
		String lowDate;
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		ObjectFactory factory = new ObjectFactory();
		String accountId = String.valueOf(accountUser.getAccount().getId());
		String path = md.getPath();

		List<MenuData> observationList = menuBean.findListContents(
				accountUser.getAccount(), msAssessments, md.getParent01());
		ObservationResults observationResults = factory
				.createEvaluatedPersonClinicalStatementsObservationResults();

		for (MenuData observation : observationList) {
			
			ObservationResult observationResult = factory
					.createObservationResult();
			observationResult.setId(setII("1.2.1." + accountId,
					observation.getPath())); 
			observationResult.setObservationFocus(setCD("1", "2.16.840.1.113883.6.56", "UMLS", observation.getString03(), observation.getString01())); // code and codeSystem appeared as null
			
			highDate = df.format(observation.getDate01());
			lowDate = df.format(observation.getDate01());
			observationResult.setObservationEventTime(setIVLTS(null, null,
					highDate, lowDate));
			observationResults.getObservationResult().add(observationResult);
		}
		if (path.contains(md.getParentPath01() + ":assessment")) {
			ObservationResult observationResult = factory
					.createObservationResult();
			observationResult.setId(setII("1.2.1." + accountId, md.getPath()));
			observationResult
					.setObservationFocus(setCD("1", "2.16.840.1.113883.6.56", "2007AA",
							md.getString03(), md.getString01()));
			highDate = df.format(md.getDate01());
			lowDate = df.format(md.getDate01());

			observationResult.setObservationEventTime(setIVLTS(null, null,
					highDate, lowDate));
			observationResults.getObservationResult().add(observationResult);
		}

		return observationResults;
	}

	

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param procedureEvents
	 * @param observationResults
	 * @param problems
	 * @return
	 */
	ClinicalStatements buildVmrClinicalStatements(
			ProcedureEvents procedureEvents,
			ObservationResults laboratoryResults, 
			Problems problems, 
			ObservationResults assessmentsEvents) {

		ObjectFactory factory = new ObjectFactory();

		ObservationResults observationResults = factory.createEvaluatedPersonClinicalStatementsObservationResults();
		observationResults.getObservationResult().addAll(laboratoryResults.getObservationResult());
		observationResults.getObservationResult().addAll(assessmentsEvents.getObservationResult());
		
		ClinicalStatements clinicalStatements = factory
				.createEvaluatedPersonClinicalStatements();
		clinicalStatements.setProcedureEvents(procedureEvents);		
		clinicalStatements.setObservationResults(observationResults);
		clinicalStatements.setProblems(problems);
		return clinicalStatements;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param df
	 * @param md
	 * @return
	 */
	Demographics buildVmrDemographics(DateFormat df, MenuData md) {

		ObjectFactory factory = new ObjectFactory();

		Demographics demographics = factory.createEvaluatedPersonDemographics();
		String birthDate = df.format(md.getParent01().getDate01());
		demographics.setBirthTime(setTS(birthDate));
		demographics.setGender(setCD(md.getParent01().getString04(),
				"2.16.840.1.113883.5", "HL7", md.getParent01().getString04(),
				null));
		return demographics;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param accountId
	 * @param md
	 * @param demographics
	 * @param clinicalStatements
	 * @return
	 */
	EvaluatedPerson buildVmrEvaluatedPerson(String accountId, MenuData md,
			Demographics demographics, ClinicalStatements clinicalStatements) {

		ObjectFactory factory = new ObjectFactory();
		EvaluatedPerson patient = factory.createEvaluatedPerson();
		
		// if there is a MRN number add it as a patient id
		if (md.getParent01().getString05() != null){
			patient.setId(setII("1.2.1." + accountId, md.getParent01().getString05()));
		}
		else{
			patient.setId(setII("1.2.1." + accountId, md.getParentPath01()));
		}
		
		patient.setDemographics(demographics);
		patient.setClinicalStatements(clinicalStatements);
		return patient;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param patient
	 * @return
	 */
	VMR buildVmrInput(EvaluatedPerson patient) {

		ObjectFactory factory = new ObjectFactory();

		VMR vmrInput = factory.createVMR();
		vmrInput.getTemplateId().add(
				setII("2.16.840.1.113883.3.1829.11.1.2.1", null));
		vmrInput.setPatient(patient);
		return vmrInput;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param vmrInput
	 * @return
	 */
	CDSInput buildCDSInput(VMR vmrInput) {

		ObjectFactory factory = new ObjectFactory();

		CDSInput cdsInput = factory.createCDSInput();
		cdsInput.getTemplateId().add(
				setII("2.16.840.1.113883.3.1829.11.1.1.1", null));
		cdsInput.setVmrInput(vmrInput);
		return cdsInput;
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param accountUser
	 * @param md
	 * @param msLabresults
	 * @param msProcedureList
	 * @param msEncounters
	 * @param msProblems
	 * @param msAssessments
	 * 
	 */
	public void opencdsServiceRequest(
			AccountUser accountUser, 
			MenuData md,
			MenuStructure msLabresults, 
			MenuStructure msProcedures,
			MenuStructure msProblems, 
			MenuStructure msAssessments)
			throws Exception, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {

		ObjectFactory factory = new ObjectFactory();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		// Build vMR payload
		Problems problems = buildVmrProblems(accountUser, md, msProblems);
		ObservationResults labResults = buildVmrLabResults(accountUser, md, msLabresults);
		ProcedureEvents procedureEvents = buildVmrProcedureEvents(accountUser,md, msProcedures);
		ObservationResults assessmentEvents = buildVmrAssessments(accountUser, md, msAssessments);

		ClinicalStatements clinicalStatements = buildVmrClinicalStatements(
				procedureEvents, labResults, problems, assessmentEvents);
		
		Demographics demographics = buildVmrDemographics(df, md);
		String accountId = String.valueOf(accountUser.getAccount().getId());
		EvaluatedPerson patient = buildVmrEvaluatedPerson(accountId, md,
				demographics, clinicalStatements);

		VMR vmrInput = buildVmrInput(patient);
		CDSInput cdsInput = buildCDSInput(vmrInput);

		JAXBContext context = JAXBContext
				.newInstance("org.opencds.tolven.plugin.cdsinput");
		JAXBElement<CDSInput> element = factory.createCdsInput(cdsInput);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter st = new StringWriter();
		marshaller.marshal(element, st);

		sendMessage(st.toString());

	}

	/**
	 * This method sends a vMR message to OpenCDS
	 * 
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * @param String
	 * @throws Exception
	 *             added on
	 */
	private void sendMessage(String base64EncodedPayload) throws Exception,
			ParserConfigurationException, SAXException, IOException,
			XPathExpressionException {

		// save file for debug purposes
		saveXmlFile(base64EncodedPayload, "payloadOut", new File(
				"/home/opencds/DebugSupport/TolvenPayloadOut"));
		EvaluateAtSpecifiedTimeResponse _evaluateAtSpecifiedTime__return = decisionSupportService(base64EncodedPayload);
		EvaluationResponse evaluationResponse = _evaluateAtSpecifiedTime__return
				.getEvaluationResponse();
		List<FinalKMEvaluationResponse> finalKMEvaluationResponse = evaluationResponse
				.getFinalKMEvaluationResponse();
		System.out.println("<base64EncodePayload>");
		List<byte[]> base64Encode = finalKMEvaluationResponse.get(0)
				.getKmEvaluationResultData().get(0).getData()
				.getBase64EncodedPayload();

		// Save base64Encode for debug purposes
		@SuppressWarnings("deprecation")
		String base64String = IOUtils.toString(base64Encode.get(0), "UTF-8");
		System.out.println(new String(base64String));
		saveXmlFile(base64String, "payloadIn", new File(
				"/home/opencds/DebugSupport/TolvenPayloadIn"));
		InputStream base64EncodeInputStream = new ByteArrayInputStream(
				base64Encode.get(0));
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(base64EncodeInputStream);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nl = (NodeList) xpath
				.evaluate(
						"//clinicalStatements/observationResults/observationResult/observationValue/concept/@displayName",
						doc, XPathConstants.NODESET);

		// Write the attributes to the Tolven Alerts list
		if (nl.getLength() > 0) {
			String displayName = ((Attr) nl.item(0)).getValue();
			setTitle(displayName);

		} else {
			setTitle("OpenCDS : no recommendation");

		}
	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param base64EncodedPayload
	 * @return
	 * @throws Exception
	 */
	private EvaluateAtSpecifiedTimeResponse decisionSupportService(
			String base64EncodedPayload) throws Exception {

		URL wsdlURL = DecisionSupportService.WSDL_LOCATION;
		DecisionSupportService ss = new DecisionSupportService(wsdlURL,
				SERVICE_NAME);
		Evaluation port = ss.getEvaluate();
		System.out.println("Invoking evaluateAtSpecifiedTime...");
		// interationId element
		org.omg.spec.cdss._201105.dss.InteractionIdentifier _evaluateAtSpecifiedTime_interactionId = new org.omg.spec.cdss._201105.dss.InteractionIdentifier();
		// Set interationId Attributes
		// scopingEntityId
		_evaluateAtSpecifiedTime_interactionId
				.setScopingEntityId("edu.utah.bmi");
		// interationId
		_evaluateAtSpecifiedTime_interactionId
				.setInteractionId("InteractionId-1754138899");
		// submissionTime
		_evaluateAtSpecifiedTime_interactionId
				.setSubmissionTime(javax.xml.datatype.DatatypeFactory
						.newInstance().newXMLGregorianCalendar(
								"2012-11-14T17:49:32.567Z"));
		// set specifiedTime element
		javax.xml.datatype.XMLGregorianCalendar _evaluateAtSpecifiedTime_specifiedTime = javax.xml.datatype.DatatypeFactory
				.newInstance().newXMLGregorianCalendar(
						"2012-11-14T17:49:32.568Z");
		// set evaluationRequest Element
		// clientLanguage
		org.omg.spec.cdss._201105.dss.EvaluationRequest _evaluateAtSpecifiedTime_evaluationRequest = new org.omg.spec.cdss._201105.dss.EvaluationRequest();
		_evaluateAtSpecifiedTime_evaluationRequest.setClientLanguage("");
		// clientTimeZoneOffset
		_evaluateAtSpecifiedTime_evaluationRequest.setClientTimeZoneOffset("");
		// kmWEvaluationRequest element
		java.util.List<org.omg.spec.cdss._201105.dss.KMEvaluationRequest> _evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequest = new java.util.ArrayList<org.omg.spec.cdss._201105.dss.KMEvaluationRequest>();
		// 1 or more repetitions.. kmEvaluatonRequest element
		org.omg.spec.cdss._201105.dss.KMEvaluationRequest _evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1 = new org.omg.spec.cdss._201105.dss.KMEvaluationRequest();
		// sub element of kmEvaluationRequest.. kmId
		org.omg.spec.cdss._201105.dss.EntityIdentifier _evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1KmId = new org.omg.spec.cdss._201105.dss.EntityIdentifier();
		// 1 or more repetitions.. dataRequirementItemData element
		// set sub element of kmEvaluationRequest.. set attribute
		// scopingEntityId
		_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1KmId
				.setScopingEntityId("org.opencds");
		// set attribute businessId
		_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1KmId
				.setBusinessId("WGS_v1");
		// set attribute version
		_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1KmId
				.setVersion("latest");
		_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1
				.setKmId(_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1KmId);
		_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequest
				.add(_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequestVal1);
		_evaluateAtSpecifiedTime_evaluationRequest
				.getKmEvaluationRequest()
				.addAll(_evaluateAtSpecifiedTime_evaluationRequestKmEvaluationRequest);
		// set attribute dataRequirementItemData
		// 1 or more repetitions
		java.util.List<org.omg.spec.cdss._201105.dss.DataRequirementItemData> _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemData = new java.util.ArrayList<org.omg.spec.cdss._201105.dss.DataRequirementItemData>();
		org.omg.spec.cdss._201105.dss.DataRequirementItemData _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1 = new org.omg.spec.cdss._201105.dss.DataRequirementItemData();
		org.omg.spec.cdss._201105.dss.ItemIdentifier _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriId = new org.omg.spec.cdss._201105.dss.ItemIdentifier();
		org.omg.spec.cdss._201105.dss.EntityIdentifier _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriIdContainingEntityId = new org.omg.spec.cdss._201105.dss.EntityIdentifier();
		// scopingEntityId
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriIdContainingEntityId
				.setScopingEntityId("edu.utah.bmi");
		// businessId
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriIdContainingEntityId
				.setBusinessId("123.456.7.8.2.1");
		// version
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriIdContainingEntityId
				.setVersion("0.2.3");
		// drild attribute
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriId
				.setContainingEntityId(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriIdContainingEntityId);
		// set itemId attribute
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriId
				.setItemId("testPayload");

		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1
				.setDriId(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DriId);
		org.omg.spec.cdss._201105.dss.SemanticPayload _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1Data = new org.omg.spec.cdss._201105.dss.SemanticPayload();
		org.omg.spec.cdss._201105.dss.EntityIdentifier _evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DataInformationModelSSId = new org.omg.spec.cdss._201105.dss.EntityIdentifier();
		// set informationModelSSId
		// scopingEntity
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DataInformationModelSSId
				.setScopingEntityId("org.opencds.vmr");
		// businessId
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DataInformationModelSSId
				.setBusinessId("VMR");
		// version
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DataInformationModelSSId
				.setVersion("1.0");
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1Data
				.setInformationModelSSId(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1DataInformationModelSSId);
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1Data
				.getBase64EncodedPayload().add(base64EncodedPayload.getBytes());
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1
				.setData(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1Data);
		_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemData
				.add(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemDataVal1);
		_evaluateAtSpecifiedTime_evaluationRequest
				.getDataRequirementItemData()
				.addAll(_evaluateAtSpecifiedTime_evaluationRequestDataRequirementItemData);
		org.omg.spec.cdss._201105.dss.EvaluateAtSpecifiedTime _evaluateAtSpecifiedTime_parameters = new EvaluateAtSpecifiedTime();
		_evaluateAtSpecifiedTime_parameters
				.setInteractionId(_evaluateAtSpecifiedTime_interactionId);
		_evaluateAtSpecifiedTime_parameters
				.setSpecifiedTime(_evaluateAtSpecifiedTime_specifiedTime);
		_evaluateAtSpecifiedTime_parameters
				.setEvaluationRequest(_evaluateAtSpecifiedTime_evaluationRequest);
		EvaluateAtSpecifiedTimeResponse _evaluateAtSpecifiedTime__return = port
				.evaluateAtSpecifiedTime(_evaluateAtSpecifiedTime_parameters);
		return _evaluateAtSpecifiedTime__return;

	}

	/**
	 * @author "Salvador Rodriguez-Loya <sal.rodloy@gmail.com>"
	 * 
	 * @param xmlString
	 * @param fileName
	 * @param dir
	 * @throws IOException
	 */
	private static void saveXmlFile(String xmlString, String fileName, File dir)
			throws IOException {

		Writer output = null;

		if (!dir.exists()) {
			dir.mkdir();
		}

		File tagFile = new File(dir, fileName + ".xml");
		if (!tagFile.exists()) {
			tagFile.createNewFile();
		}

		output = new BufferedWriter(new FileWriter(tagFile));
		output.write(xmlString);
		output.close();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public String getReviewed() {
		return reviewed;
	}

	public void setReviewed(String reviewed) {
		this.reviewed = reviewed;
	}

	public String getSent() {
		return sent;
	}

	public void setSent(String sent) {
		this.sent = sent;
	}

	public String getRecommendedAction() {
		return recommendedAction;
	}

	public void setRecommendedAction(String recommendedAction) {
		this.recommendedAction = recommendedAction;
	}

	public String getRecommendedActionTaken() {
		return recommendedActionTaken;
	}

	public void setRecommendedActionTaken(String recommendedActionTaken) {
		this.recommendedActionTaken = recommendedActionTaken;
	}

}
