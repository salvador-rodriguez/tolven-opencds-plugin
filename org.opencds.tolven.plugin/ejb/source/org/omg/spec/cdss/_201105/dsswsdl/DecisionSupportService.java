
/*
 * 
 */

package org.omg.spec.cdss._201105.dsswsdl;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.3.1
 * Sat Nov 17 19:24:13 GMT 2012
 * Generated source version: 2.3.1
 * 
 */


@WebServiceClient(name = "DecisionSupportService", wsdlLocation = "/home/opencds/wsdl/opencds.wsdl",targetNamespace = "http://www.omg.org/spec/CDSS/201105/dssWsdl") 
public class DecisionSupportService extends Service {

    public final static URL WSDL_LOCATION;
    public final static QName SERVICE = new QName("http://www.omg.org/spec/CDSS/201105/dssWsdl", "DecisionSupportService");
    public final static QName Evaluate = new QName("http://www.omg.org/spec/CDSS/201105/dssWsdl", "evaluate");
    public final static QName Query = new QName("http://www.omg.org/spec/CDSS/201105/dssWsdl", "query");
    public final static QName Metadata = new QName("http://www.omg.org/spec/CDSS/201105/dssWsdl", "metadata");
    static {
        URL url = null;
        try {
        	url = new URL("file:///home/opencds/wsdl/opencds.wsdl"); 
        } catch (MalformedURLException e) {
            System.err.println("Can not initialize the default wsdl from .../opencds-decision-support-service/evaluate?wsdl");
            e.printStackTrace();
        }
        WSDL_LOCATION = url;
    }

    public DecisionSupportService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public DecisionSupportService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public DecisionSupportService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    /*
    public DecisionSupportService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }
    public DecisionSupportService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public DecisionSupportService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }
*/
    /**
     * 
     * @return
     *     returns Evaluation
     */
    @WebEndpoint(name = "evaluate")
    public Evaluation getEvaluate() {
        return super.getPort(Evaluate, Evaluation.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Evaluation
     */
    @WebEndpoint(name = "evaluate")
    public Evaluation getEvaluate(WebServiceFeature... features) {
        return super.getPort(Evaluate, Evaluation.class, features);
    }
    /**
     * 
     * @return
     *     returns Query
     */
    @WebEndpoint(name = "query")
    public Query getQuery() {
        return super.getPort(Query, Query.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Query
     */
    @WebEndpoint(name = "query")
    public Query getQuery(WebServiceFeature... features) {
        return super.getPort(Query, Query.class, features);
    }
    /**
     * 
     * @return
     *     returns MetadataDiscovery
     */
    @WebEndpoint(name = "metadata")
    public MetadataDiscovery getMetadata() {
        return super.getPort(Metadata, MetadataDiscovery.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns MetadataDiscovery
     */
    @WebEndpoint(name = "metadata")
    public MetadataDiscovery getMetadata(WebServiceFeature... features) {
        return super.getPort(Metadata, MetadataDiscovery.class, features);
    }

}
